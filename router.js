const getRoot = () => location.origin + "/aa/b/ccc/"

const getRoute = url => {
	const root = getRoot()
	if(!url.href.startsWith(root)) throw new Error("url is out of root")
	const path = url.href.slice(root.length)
	let module_url = root + "pages/" + path
	module_url += module_url.endsWith("/") ? "index.js" : ".js"

	return { params: {}, module_url }
}

const getNotFoundRoute = url => {
	return { module_url: getRoot() + "pages/not-found.js" }
}

const getErrorRoute = err => {
	return { module_url: getRoot() + "pages/error.js" }
}

export default { getRoute, getRoot, getNotFoundRoute, getErrorRoute }
