import { html, render } from "https://unpkg.com/lit-html"

export default parts => html`
	<section>
		<header>
			<a href=".">Top</a>
			<a href="page1">page1</a>
			<a href="page2">page2</a>
			<a href="foo/bar">foo/bar</a>
		</header>
		<main>${parts.body}</main>
	</section>
`
