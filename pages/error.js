import { html, render } from "https://unpkg.com/lit-html"

const renderPage = ({ error }) => {
	document.title = "Error"
	render(
		html`
			<h1>Error</h1>
			<pre>${error.stack}</pre>
		`,
		document.body
	)
}

export { renderPage as render }
