import { html, render } from "https://unpkg.com/lit-html"
import tpl from "../tpl.js"

const renderPage = () => {
	document.title = "foo/bar"
	render(
		tpl({ body: html`<h1>foo/bar</h1>` }),
		document.body
	)
}

export { renderPage as render }
