import { html, render } from "https://unpkg.com/lit-html"

const renderPage = () => {
	document.title = "NotFound"
	render(
		html`
			<h1>NOT FOUND</h1>
		`,
		document.body
	)
}

export { renderPage as render }
