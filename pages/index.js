import { html, render } from "https://unpkg.com/lit-html"
import tpl from "./tpl.js"

const renderPage = () => {
	document.title = "Top"
	render(
		tpl({ body: html`<h1>Top</h1>` }),
		document.body
	)
}

export { renderPage as render }
