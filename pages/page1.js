import { html, render } from "https://unpkg.com/lit-html"
import tpl from "./tpl.js"

const renderPage = () => {
	document.title = "page1"
	render(
		tpl({ body: html`<h1>page1</h1>` }),
		document.body
	)
}

export { renderPage as render }
