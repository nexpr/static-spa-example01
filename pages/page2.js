import { html, render } from "https://unpkg.com/lit-html"
import tpl from "./tpl.js"

const renderPage = () => {
	document.title = "page2"
	const data = [1, 2, 3, 4, 1, 2]
	render(
		tpl({
			body: html`
				<h1>pag${null[1]}e2</h1>
				<ul>${data.map(e => html`<li>${e}</li>`)}</ul>
			`
		}),
		document.body
	)
}

export { renderPage as render }
