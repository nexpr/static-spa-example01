# static spa example 1

WebComponents や framework を使ってると HTML はルートのみだったりで JavaScript で HTML を全部作ることが多いので ロード対象は JavaScript のみ  
常に `index.html` を表示して URL に応じて `.js` ファイルを module としてロード  
モジュールの `render` 関数を実行してページを初期化する  
`document.title` や `document.body` の更新など  

デフォルトでは `pages` フォルダ以下のパスと URL のパスを対応づけてる  
root が `/` なら `/foo/bar` は `/foo/bar.js` になって `/` で終わると `/index.js`  

詳細な設定は `router.js` で行う  
`{getRoute, getRoot, getNotFoundRoute, getErrorRoute}`  
これらの関数を default に export する  

- `getRoot` はアプリケーションの root の URL を文字列で返す関数  
  `/foo/bar/` など `/` に配置しない場合  
- `getRoute` は URL オブジェクトから route 情報を返す関数  
  返す値はオブジェクトで `module_url` プロパティが必須  
  `module_url` は import するモジュールの URL  
  必要なら `params` プロパティを追加して `render` で受け取れる  
- `getNotFoundRoute` は `getRoute` のモジュールが見つからなかったら呼び出される  
  返すオブジェクトは `getRoute` と同じ  
  404 ページ用  
- `getErrorRoute` は `getNotFoundRoute` の処理やモジュールの `render` 処理でエラーが起きると呼び出される  
  返すオブジェクトは `getRoute` と同じ  
  500 ページ用  

ここを変えてカスタマイズする  
`pages` 内のサンプルのモジュールでは lit-html を使ってるけど ただの JavaScript や他の framework でも大丈夫  
